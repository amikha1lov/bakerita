"use strict";

var app = new Vue({
  el: '#app',
  created: function () {
    this.loadProducts();    
  },
  data: {
    products: []
  },
  methods: {
    // Функция получает список товаров для галереи.
    loadProducts: function () {
      const sUrl = "https://bakeritadogs.xyz/api/data/product/display-products";

      axios.post(sUrl)
        .then(res => {
          this.products = res.data;
          console.log("products", this.products);
        })
        .catch(err => {
          console.log("error displaying products", err);
        });

        CommonMethods.SendCookie();
    }
  }
});

var app_2 = new Vue({
  el: '#app_2',
  created: function () {
    $(".hidden").hide();
    this.loadBestOffers();
  },
  data: {
    offers: []
  },
  methods: {
    // Функция получает список лучших предложений.
    loadBestOffers: function () {
      const sUrl = "https://bakeritadogs.xyz/api/data/product/display-best-offers";

      axios.post(sUrl)
        .then(res => {
          this.offers = res.data;
          console.log("offers", this.offers);
        })
        .catch(err => {
          console.log("error displaying offers", err);
        });
    },

    // Функция добавляет товар в корзину.
    onAddToBasket(id) {
      Basket.AddProductToBasket(id);
      console.log("basket add");
    }
  }
});

var app_3 = new Vue({
  el: '#app_3',
  created: function () {
    this.loadNews();
  },
  data: {
    news: []
  },
  methods: {
    // Функция получает список новостей. 
    loadNews: function () {
      const sUrl = "https://bakeritadogs.xyz/api/data/news/get-news";

      axios.get(sUrl)
        .then(res => {
          this.news = res.data;
          console.log("news", this.news);
        })
        .catch(err => {
          console.log("error displaying news", err);
        });
    }
  }
});
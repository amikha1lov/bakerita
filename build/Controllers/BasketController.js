"use strict";

class Basket {
    // Метод добавляет товар в корзину.
    static AddProductToBasket(id) {
        const sUrl = "https://bakeritadogs.xyz/api/data/product/add-to-basket";

        let oData = {
            ProductId: id,
            Cookie: document.cookie
        };

        axios.post(sUrl, oData)
        .then(res => {
          console.log(res);
        })
        .catch(err => {
          console.log("error adding to basket", err);
        });
    }
}
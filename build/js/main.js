$(document).ready(function() {
	setTabs();
	slider__offers();
	slider__news();
	slider();
	modal();
	windowSize();
});

$(window).resize(function() {
	windowSize();
});


/* Tabs */
function setTabs() {
	$(".tabs").on("click", ".tabs__item", function(){
		var tabs = $(".tabs .tabs__item"),
			cont = $(".tabs__content");
		tabs.removeClass("tabs__item--active");
		cont.removeClass("tabs__content--active");
		$(this).addClass("tabs__item--active");
		cont.eq($(this).index()).addClass("tabs__content--active");
		return false;
	});
}

function slider() {
	$('.slider').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		arrows: true,
		responsive: [{
			breakpoint: 1199,
			settings: {
				dots: true,
				arrows: false,
			}
		}]
	});
}

function slider__offers() {
	$('.slider-offers__block').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		adaptiveHeight: true,
		arrows: true,
		responsive: [{
			breakpoint: 1199,
			settings: 'unslick'
		}]
	});
}

function slider__news() {
	$('.slider__news').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		adaptiveHeight: true,
		arrows: true,
		responsive: [{
			breakpoint: 1199,
			settings: 'unslick'
		}]
	});
}

function modal() {
	$('.js-showModal').on('click', function () {
		$('#myModal-1').addClass('active')
		$('body').addClass('off-scroll');
	})

	$(".js-closeModal").on("click", function () {
		$('#myModal-1').removeClass("active")
		$("body").removeClass("off-scroll");
	})
}

function windowSize() {
                  
	let w = window.innerWidth;
	
	if (w <= 1199) {

		$('.js-showModal-2').on('click', function () {
			$('#myModal-2').find('.gallery__menu-item__image').remove();
			$('#myModal-2').find('.gallery__menu-item__content').remove();
			$('#myModal-2').find('.slider-offers__item-image').remove();
            $('#myModal-2').addClass('active')
			$('body').addClass('off-scroll');
			$(this).clone().appendTo('.modal__gallery-image').show();
			$(this).next('.gallery__menu-item__content').clone().appendTo('.modal__gallery-content').show();
			modal();
        })
        $(".js-closeModal-2").on("click", function () {
            $('#myModal-2').removeClass("active")
			$("body").removeClass("off-scroll");
		})

		$('.js-showModal-3').on('click', function () {
			$('#myModal-3').find('.gallery__menu-item__image').remove();
			$('#myModal-3').find('.gallery__menu-item__content').remove();
			$('#myModal-3').find('.slider-offers__item-image').remove();
            $('#myModal-3').addClass('active')
			$('body').addClass('off-scroll');
			$(this).clone().appendTo('.modal__gallery-image').show();
        })
        $(".js-closeModal-3").on("click", function () {
            $('#myModal-3').removeClass("active")
			$("body").removeClass("off-scroll");
		})

		$('.modal__more').on('click', function() {
			$('#myModal-2').find('.modal__gallery-content').toggleClass('active')
		})

	} else {
		$('.gallery__menu-item').hover(function () {
			$(this).toggleClass('active');
		});
	}

}